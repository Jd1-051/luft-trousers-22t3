﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Provides a layer of separation between the needs of the spawning system and the logic required to determine
    /// valid locations for it to use.
    /// </summary>
    public class SpawnLocationProvider : MonoBehaviour
    {
        [SerializeField] protected Vector2 heightRange;
        [SerializeField] protected Vector2 playerDistRange;
        [SerializeField] protected GameObjectCollectionSO playerCollection;

        protected Vector2 objectSize;
        protected Rect worldViewRect;
        private Vector3 lastProposedPos;

        //possibly overkill, but allows us to init once per frame rather than every spawn request
        public virtual void InitPreFetch(Vector2 objectSize, Camera camera)
        {
            this.objectSize = objectSize;
            //nudge outside view
            var minP = camera.ViewportToWorldPoint(Vector3.zero) - (Vector3)objectSize;
            var maxP = camera.ViewportToWorldPoint(Vector3.one) + (Vector3)objectSize;
            worldViewRect = new Rect();
            worldViewRect.min = minP;
            worldViewRect.max = maxP;
        }

        public virtual Vector3 FetchCameraSafeRandom(Camera camera)
        {
            var y = Random.Range(heightRange.x, heightRange.y);
            var z = 0;
            var p = playerCollection.First;
            if (p == null)
                return lastProposedPos;

            //start from player pos,
            //left or right
            var facing = Mathf.Sign(Vector3.Dot(Vector3.left, p.transform.up));
            //2 to 1 weighting
            var side = Random.Range(-2, 1) * facing;
            var pdist = Random.Range(playerDistRange.x, playerDistRange.y);
            var x = pdist * facing * Mathf.Sign(side);
            var camX = camera.transform.position.x;
            var proposedPos = new Vector3(x + camX, y, z);

            //if the player might see us, as we are too close to the view rect, we move outside it
            if (worldViewRect.Contains(proposedPos))
            {
                if (proposedPos.x < worldViewRect.center.x)
                {
                    proposedPos.x = worldViewRect.min.x - objectSize.x;//pad
                }
                else
                {
                    proposedPos.x = worldViewRect.max.x + objectSize.x;//pad
                }
            }

            lastProposedPos = proposedPos;

            return proposedPos;
        }

        private void OnDrawGizmosSelected()
        {
            var curCol = Gizmos.color;
            Gizmos.color = Color.yellow;
            var p = Camera.main.transform.position;
            var centre = new Vector3(p.x, (heightRange.x + heightRange.y) / 2, 0);
            Gizmos.DrawWireCube(centre, new Vector3(playerDistRange.y * 2, heightRange.y - heightRange.x, 1));
            Gizmos.color = curCol;
        }
    }
}
