using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAE;

public class Brick : Weapon
{
    [SerializeField] private int Ammo;
    [SerializeField] private int startingAmmo;

    [SerializeField]float reloadTime = 5;
    float currentTimeTillReload;

    public override void Start()
    {
        base.Start();
        startingAmmo = Ammo;
        currentTimeTillReload = Time.time + reloadTime;
    }

    public override void Fire()
    {
        //at present we assume 'low' firerates so no more than 1 per frame and no accounting for per frame
        //  delta fluctuation.
        if (CanFire())
        {
            fireRateCounter += projectilePrefab.FireRate;
            Ammo--;
            CreateNewProjectile();
            DoFireEffects();
            Debug.Log("reload1");
            if (Ammo <= 0)
            {
                currentTimeTillReload = Time.time + reloadTime;
                // played a reload sfx here.
                Debug.Log("reload2");
            }
        }
    }

    public override void Update()
    {
        base.Update();
        if (Ammo <= 0)
        {
            Debug.Log("reload3");
            // reload
            if (Time.time > currentTimeTillReload)
            {
                Debug.Log("Reload4");
                Ammo = startingAmmo;
                currentTimeTillReload = Time.time + reloadTime;
            }
        }
    }

    protected override bool CanFire()
    {
        return fireRateCounter < 0 && (Ammo > 0);
    }

}
