﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Handles creation and rate limiting of projectiles. Intended for use by player and enemies.
    /// </summary>
    public class Weapon : MonoBehaviour
    {
        [Tooltip("If null, self is used")]
        [SerializeField] protected Transform spawnLocation;

        [SerializeField] protected Projectile projectilePrefab;
        protected float fireRateCounter;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected bool projectilesInheritVelocity = true;
        [SerializeField] protected Rigidbody2D rb2D;

        [ContextMenu("FireNow")]
        public virtual void Fire()
        {
            //at present we assume 'low' firerates so no more than 1 per frame and no accounting for per frame
            //  delta fluctuation.
            if (CanFire())
            {
                fireRateCounter += projectilePrefab.FireRate;
                CreateNewProjectile();
                DoFireEffects();
            }
        }

        public virtual void SetProjectile(Projectile proj)
        {
            projectilePrefab = proj;
            fireRateCounter = 0;
        }

        public virtual void Start()
        {
            if (spawnLocation == null)
                spawnLocation = transform;
        }

        public virtual void Update()
        {
            if (fireRateCounter >= 0)
            {
                fireRateCounter -= Time.deltaTime;
            }
        }
        protected virtual bool CanFire()
        {
            return fireRateCounter < 0;
        }

        protected virtual void CreateNewProjectile()
        {
            var newProj = Instantiate(projectilePrefab.gameObject, spawnLocation.position, spawnLocation.rotation).GetComponent<Projectile>();

            if (projectilesInheritVelocity && rb2D != null)
            {
                newProj.Rigidbody2D.velocity += rb2D.velocity;
            }
        }

        protected virtual void DoFireEffects()
        {
            if (audioSource != null &&
                projectilePrefab.ClipToPlayOnFire != null)
            {
                audioSource.PlayOneShot(projectilePrefab.ClipToPlayOnFire);
            }

            if (projectilePrefab.SpawnEffect != null)
            {
                Instantiate(projectilePrefab.SpawnEffect, spawnLocation.position, spawnLocation.rotation);
            }
        }
    }
}