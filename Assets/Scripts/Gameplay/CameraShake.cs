using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAE;

//Nathan comment: a summary would be lovely here.
public class CameraShake : MonoBehaviour
{
    [SerializeField] protected Transform cam; 
    [SerializeField] protected Vector3 offset = new Vector3(0, 0, 0); 
    [SerializeField] protected float shakeMagnitude; 
    [SerializeField] protected float dampingSpeed; 
    [SerializeField] protected float shakeDuration; 
    [SerializeField] protected float endShakeDuration; 
    protected bool isShaking; 
    protected Rigidbody2D targetRB2D; 
    protected Vector3 initialPosition; 

    private void OnEnable()
    {
        EventsManager.onFloatShakeEffect += TriggerShake; 
    }

    private void OnDisable()
    {
        EventsManager.onFloatShakeEffect -= TriggerShake; 
    }

    void Start()
    {
        StartFixedPosition();
    }
    
    void Update()
    {
        //Nathan comment: Could we have this in a function call instead?
        if (shakeDuration > 0)
        {
            ShakeIt();
        }
        else
        {
            StopShaking();
        }
    }

   //Nathan comment: a summary would be lovely here.
   // consider renaming this fucntion to something like set up, or find references?
    protected void StartFixedPosition()
    {
        cam = gameObject.transform;
        targetRB2D = FindObjectOfType<Health>().gameObject.GetComponent<Rigidbody2D>();
    }

    //Nathan comment: a summary would be lovely here.
    /// <param name="newDuration"></param>
    /// <param name="newMagnitude"></param>
    protected void TriggerShake(float newDuration, float newMagnitude)
    {
        isShaking = true;
        initialPosition = transform.position;
        shakeDuration = newDuration;
        shakeMagnitude = newMagnitude;
    }

   //Nathan comment: a summary would be lovely here.
    protected void ShakeIt()
    {
        //Nathan comment: if debug is no longer required comment it out.
        Debug.Log("shaking");
        transform.position = targetRB2D.transform.position + offset + Random.insideUnitSphere * shakeMagnitude;
        shakeDuration -= Time.deltaTime * dampingSpeed;
    }

    //Nathan comment: a summary would be lovely here.
    protected void StopShaking()
    {
        isShaking = false;
        shakeDuration = endShakeDuration;
        transform.position = targetRB2D.transform.position + offset;
    }
}
