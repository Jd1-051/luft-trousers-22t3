﻿using UnityEngine;

namespace SAE
{
    /// <summary>
    /// A Starting point for a Health script, intended to be expanded to meet indivdual games needs, either via
    /// direct modification or inheritence.
    /// </summary>
    public class Health : MonoBehaviour
    {
        [SerializeField] protected float hp;
        [SerializeField] protected float maxHp;
        [SerializeField] protected bool isAlive = true;
        [SerializeField] protected bool isVuln = true;

        [SerializeField] protected AudioEffectSO deathAudioEffect;
        [SerializeField] protected GameObject deathEffectPrefab;
        [SerializeField] protected float shakeTime, shakeAmount;

        private void OnTriggerEnter2D(Collider2D other)
        {
            
        }
        public virtual void Start()
        {
            hp = maxHp;
        }

        public virtual void ReceiveDamage(float dmg)
        {
            if (!isAlive && !isVuln)
                return;

            hp -= dmg;
            OnDamage();

            if (hp <= 0)
            {
                isAlive = false;
                Death();
            }
        }

        public virtual void AddHP(float amt)
        {
            hp += amt;
            hp = Mathf.Clamp(hp, 0, maxHp);
        }

        protected virtual void Death()
        {
            Destroy(gameObject);
            DoDeathEffects();
        }

        protected virtual void DoDeathEffects()
        {
            if (deathEffectPrefab != null)
            {
                Instantiate(deathEffectPrefab, transform.position, deathEffectPrefab.transform.rotation * transform.rotation);
            }

            if (deathAudioEffect != null)
            {
                deathAudioEffect.Play2D();
            }
        }

        // this is a good starting point, but eventually you might want to pass more context
        protected virtual void OnDamage()
        {
            EventsManager.onFloatShakeEffect?.Invoke(shakeAmount, shakeTime);
        }
    }
}
