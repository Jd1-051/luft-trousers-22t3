using UnityEngine;

namespace SAE.Example
{
    //Nathan comment: Let's update this summary below to better match what we have here now..

    /// <summary>
    /// Simple offset style camera follow, to be replaced.
    /// </summary>
    public class CameraFollowRB2D : MonoBehaviour
    {
        //Positional Camera Script
        [SerializeField] protected Transform targetRB2D; // Transform to follow.
        [SerializeField] protected float dampTime; // to offset in a 'trailing' behind, as it catches back up to player.
        [SerializeField] protected Vector3 offset = new Vector3(0, 0, 0);
        protected Vector3 velocity = Vector3.zero; // for setting velocity of camera follow

        void Update()
        {
        
            //Nathan comment: if we no longer are using this code, let's remove it.
            
            //Positional Camera Script
            //cameraTransform.position = targetRB2D.transform.position + offset;
        
            //Transform.Translate script
            //transform.Translate(0, 0, Time.deltaTime);
            //transform.Translate(0, Time.deltaTime, 0);
             CameraPosSet();
        }


        //Nathan comment: a summary would be lovely here.

        void CameraPosSet()
        {
            Vector3 targetPosition = targetRB2D.TransformPoint(offset); // Define a target position above the target transform
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, dampTime); // Setting the camera position, witha following with a trailing effect.
        }
    }
}
