﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace SAE
{
    /// <summary>
    /// Simple gather of user input, converted to plane movement desired. This separation is intended to
    /// keep the componenets responsible for moving objects clean and free of player centric and/or
    /// input code so they can be resuled by NPCs/Enemies etc.
    /// </summary>
    public class PlayerInputRouter : MonoBehaviour
    {
        [SerializeField] protected PlayerInput playerInput;

        private PlayerInput PlayerController
        {
            get
            {
                if(playerInput == null)
                {
                    playerInput = GetComponent<PlayerInput>();
                    // if we somehow don't get the what we want return null
                    if(playerInput == null)
                    {
                        Debug.LogError("No Player Input Script on " + transform.name);
                        return null;
                    }
                }
                return playerInput;
            }
        }


        [SerializeField] protected PlaneMovement playerMovement;

        private PlaneMovement PlayerMovement
        {
            get
            {
                if(playerMovement == null)
                {
                    playerMovement = GetComponent<PlaneMovement>();
                    if(playerMovement == null)
                    {
                        Debug.LogError("No player movement script on " + transform.name);
                        return null;
                    }
                }
                return playerMovement;
            }
        }

        [SerializeField] protected Weapon weapon;

        private Weapon CurrentWeapon
        {
            get
            {
                if (weapon == null)
                {
                    weapon = GetComponent<Weapon>();
                    if (weapon == null)
                    {
                        Debug.LogError("No weapon script on " + transform.name);
                        return null;
                    }
                }
                return weapon;
            }
        }

        private bool firePressed; // is fire being pressed.
        private Vector2 inputVector; // the rotation.

        private void OnEnable()
        {
            PlayerController.onActionTriggered += ReadControllerInput;
        }

        private void OnDisable()
        {
            PlayerController.onActionTriggered -= ReadControllerInput;
        }

        private void Update()
        {
            if(firePressed)
            {
                CurrentWeapon.Fire();
            }

            PlayerMovement.SetEngineState(inputVector.y, -inputVector.x);
        }

        private void ReadControllerInput(InputAction.CallbackContext context)
        {
            if(context.action.name == "Fire")
            {
                // toggle example
                //if (context.performed)
                //{
                //    firePressed = !firePressed;
                //}

                if (context.performed)
                {
                    firePressed = true;
                }
                else if(context.canceled)
                {
                    firePressed = false;
                }
            }
            else if(context.action.name == "Accelerate") // thurst?
            {
                float amount = context.ReadValue<float>();

                if (context.performed)
                {
                    inputVector.y = amount;
                }
                else if (context.canceled)
                {
                    inputVector.y = 0;
                }
            }
            else if(context.action.name == "Rotate") // grab left right values
            {
                inputVector.x = context.ReadValue<Vector2>().x;
            }
        }
    }
}
