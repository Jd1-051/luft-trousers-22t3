﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SAE
{
    /// <summary>
    /// Simple helper for having a collection of AudioSources for re-use
    /// </summary>
    public class AudioSourcePool : MonoBehaviour
    {
        #region singleton stuff

        private static AudioSourcePool _inst;

        public static AudioSourcePool Inst
        {
            get
            {
                if (_inst == null)
                {
                    _inst = FindObjectOfType<AudioSourcePool>();

                    if (_inst == null)
                    {
                        _inst = new GameObject("AudioSourcePool").AddComponent<AudioSourcePool>();
                    }
                }

                return _inst;
            }
        }

        #endregion singleton stuff

        [SerializeField]
        private List<AudioSource> available = new List<AudioSource>();

        public static AudioSource GetSource()
        {
            if (Inst.available.Count == 0)
            {
                var newGO = new GameObject();
                newGO.transform.parent = Inst.transform;
                Inst.available.Add(newGO.AddComponent<AudioSource>());
            }

            var s = Inst.available.Last();
            Inst.available.RemoveAt(Inst.available.Count - 1);
            return s;
        }

        public static void ReturnSourceWhenDone(AudioSource s)
        {
            Inst.StartCoroutine(ReturnWhenDoneInternal(s));
        }

        public static void ReturnSource(AudioSource s)
        {
            Inst.available.Add(s);
        }

        private static IEnumerator ReturnWhenDoneInternal(AudioSource s)
        {
            yield return new WaitForSeconds(s.clip.length);
            ReturnSource(s);
        }
    }
}
